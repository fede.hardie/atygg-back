<?php
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    
    Route::middleware('api')->group(function () {
        Route::post('logout', 'App\Http\Controllers\AuthController@logout');
        Route::post('refresh', 'App\Http\Controllers\AuthController@refresh');
        Route::post('me', 'App\Http\Controllers\AuthController@me');
        
        //RUTAS DE USUARIO
        Route::get('getUsers', 'App\Http\Controllers\AuthController@getUsers');
        Route::post('register', 'App\Http\Controllers\AuthController@register');
        Route::get('getUser/{id}', 'App\Http\Controllers\AuthController@getUserById');
        Route::put('updateUser/{id}', 'App\Http\Controllers\AuthController@updateUser');
        Route::delete('deleteUser/{id}', 'App\Http\Controllers\AuthController@deleteUser');
    });
});
