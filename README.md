<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Paso 1

Clonar el repositorio y en la raiz ejecutar el comando "composer install" para instalar todas las dependencias

## Paso 2

Generar una base de datos con el nombre que se desee

### Paso 3

Cambiar el nombre del archivo ".env.example.env" por ".env". Dentro del archivo, modificar los siguientes campos según corresponda:
    <br>
    DB_HOST=127.0.0.1
    <br>
    DB_PORT=3306
    <br>
    DB_DATABASE=atygg
    <br>
    DB_USERNAME=root
    <br>
    DB_PASSWORD=

## Paso 4

Ejecutar el comando "php artisan migrate" para generar la estructura de la base de datos

## Paso 5

Ejecutar el comando "php artisan db:seed" para poblar la tabla users. Los datos del usuario son:
    <br>
    Email: prueba@gmail.com
    <br>
    Password: 123456

## Paso 6

Levantar el servicio con "php artisan serve"

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
