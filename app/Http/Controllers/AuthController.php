<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Person;
use App\Models\PasswordReset;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        $validator = Validator::make($credentials, [
            'email' => 'required|string|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            $data = array(
                'status' => 400,
                'message' => $validator->errors()
            );
            return response()->json($data);
        }

        if (!$token = auth()->attempt($credentials)) {

            $json = array(
                'status' => 400,
                'message' => 'Usuario y/o contraseña incorrecta'
            );

            return response()->json($json);
        }
        return $this->respondUserWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Se cerró la sesión correctamente']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 6000
        ]);
    }

    protected function respondUserWithToken($token)
    {
        $user = auth()->user();
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 6000,
            'user' =>$user,
            'status' => 201
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'lastname' => 'required|string',
            'birth_date' => 'required|date',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            $data = array(
                'status' => 400,
                'message' => $validator->errors()
            );
            return response()->json($data);
        }

        if ($validator) {
            try {
                DB::beginTransaction();

                $user = User::create([
                    'name' => $request['name'],
                    'lastname' => $request['lastname'],
                    'birth_date' => $request['birth_date'],
                    'email' => $request['email'],
                    'password' => bcrypt($request['password']),
                ]);

                DB::commit();
                $data = array(
                    'message' => 'Usuario registrado exitosamente',
                    'status' => 201,
                    'user' => $user
                );
            } catch (\Throwable $th) {
                DB::rollBack();
                $data = array(
                    'message' => $th,
                    'status' => 500
                );
            }
            return response()->json($data);
        }
    }

    public function getUsers()
    {
        $users = User::get();

        if (count($users) > 0) {
            $data = array(
                'message' => 'Listado de usuarios',
                'status' => 201,
                'cantidad' => count($users),
                'users' => $users
            );
        } else {
            $data = array(
                'message' => 'No se encuentran usuarios en el sistema',
                'status' => 400
            );
        }

        return response()->json($data);
    }

    public function getUserById($id)
    {
        $user = User::find($id);

        if ($user) {
            $data = array(
                'message' => 'Detalle de usuario',
                'status' => 201,
                'users' => $user
            );
        } else {
            $data = array(
                'message' => 'No se encuentra el usuario en el sistema',
                'status' => 400
            );
        }

        return response()->json($data);
    }

    public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'lastname' => 'required|string',
                'birth_date' => 'required|date'
            ]);

            if ($validator->fails()) {
                $data = array(
                    'status' => 400,
                    'message' => $validator->errors()
                );
                return response()->json($data);
            }

            try {
                DB::beginTransaction();

                $user->save([
                    $user->name = $request['name'],
                    $user->lastname = $request['lastname'],
                    $user->birth_date = $request['birth_date'],
                ]);

                DB::commit();
                $data = array(
                    'message' => 'Usuario actualizado correctamente',
                    'status' => 201
                );
            } catch (\Throwable $th) {
                DB::rollBack();
                $data = array(
                    'message' => $th,
                    'status' => 500
                );
            }
        } else {
            $data = array(
                'message' => 'No se encuentra el usuario en el sistema',
                'status' => 400
            );
        }
        return response()->json($data);
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        if ($user) {
            try {
                DB::beginTransaction();
                $user->delete();
                DB::commit();
                $data = array(
                    'message' => 'Usuario eliminado correctamente',
                    'status' => 201
                );
            } catch (\Throwable $th) {
                DB::rollBack();
                $data = array(
                    'message' => $th,
                    'status' => 500
                );
            }
        } else {
            $data = array(
                'message' => 'No se encuentra el usuario en el sistema',
                'status' => 400
            );
        }
        return response()->json($data);
    }
}
